package main

import (
	"encoding/json"
	"fmt"
	"unicode"
)

var (
   menuutama, menu,cek int
)

func substr(s string, start, end int) string {
	counter, startIdx := 0, 0
	for i := range s {
		if counter == start {
			startIdx = i
		}
		if counter == end {
			return s[startIdx:i]
		}
		counter++
	}
	return s[startIdx:]
}
func array_has[T comparable](haystack []T, needle T) bool {
	for _, val := range haystack {
		if val == needle {
			return true
		}
	}
	return false
}
func removeSpace(s string) string {
	rr := make([]rune, 0, len(s))
	for _, r := range s {
		if !unicode.IsSpace(r) {
			rr = append(rr, r)
		}
	}
	return string(rr)
}

func main() {
   fmt.Println("Aplikasi Tugas Golang ")
   fmt.Println("1. Nomor Telepon")
   fmt.Println("2. In Array")
   fmt.Println("3. Flip x")
   fmt.Println("4. Les Piano")
   fmt.Println("5. Enkripsi Teks")
   fmt.Println("6. Keluar")
   fmt.Println("=========================================")
   fmt.Print("Pilih salah satu menu diatas (masukkan nomor menu): ")
   fmt.Scanf("%d", &menuutama)

switch menuutama{
   case 1 :
      nomorTelpon()
   case 2 :
      ArrayIn()
   case 3 :
      flipX()
   case 4 :
      lesPiano()
   case 5 :
      enkripsiText()
   default :
      exit()
   }
}

//====================Phone======================//
func nomorTelpon(){
	var phone string
	fmt.Println("Masukkan Phone Anda : ")
	fmt.Scan(&phone)
	Phones:=""
	if substr(phone, 0, 5) == "0857-"{
		Phones = substr(phone,0, 5)+substr(phone,5, 9)+substr(phone,9, 14);
	}else if substr(phone, 0, 1) == "0"{
		Phones = "0"+substr(phone,1, 4)+"-"+substr(phone,4, 8)+"-"+substr(phone,8, 12);
	}else if substr(phone, 0, 1) == "8"{
		Phones = "0"+substr(phone,0, 4)+"-"+substr(phone,4, 8)+"-"+substr(phone,8, 12);
	}else {
		Phones = phone;
	}
	fmt.Println("Output ", Phones)
   main()
}

//=======================Array In====================//
func ArrayIn(){

//    fmt.Println("Menghitung Luas Segitiga")
//    fmt.Print("Masukkan alas segitiga : ")
//    fmt.Scanf("%f", &alas)
//    fmt.Print("Masukkan tinggi segitiga : ")
//    fmt.Scanf("%f", &tinggi)
//    luas := (alas*tinggi)/2
//    fmt.Println("Luas Persegi= ", luas)
	var keyword string
	fmt.Println("Masukkan Keyword Anda : ")
	fmt.Scan(&keyword)

	var inArray = []string{"FrontEnd", "BackEnd"}

	

	var jsonData, err = json.Marshal(inArray)
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	var jsonString = string(jsonData)
	fmt.Println(jsonString)
	fmt.Println(array_has(inArray, keyword))
   main()
}

//==================Flip X======================//
func flipX(){
  fmt.Println("ini belum")
   main()
}

//============Les Piano=========//
func lesPiano(){
   fmt.Println("ini belum")

   main()
}

//======================Enkripsi Text======================//
func enkripsiText(){
	s := "aku wibu dan aku bangga"
	fmt.Println(s)
	s = removeSpace(s)
	
	fmt.Println(s)
	fmt.Println(len(s))
	for pos, char := range s {
		fmt.Printf("character %c starts at byte position %d\n", char, pos)
	}
   main()
}

//=========================EXIT=====================//
func exit(){
   fmt.Println("====Selesai===")
}